import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.io.Serializable;


public class MerkleTree implements Serializable {

    private class Node implements Serializable {
	    byte[] hash;
	    Node left;
	    Node right;
	    Node parent;

	    Node(byte[] hash, Node left, Node right) {
            this.hash = hash;
            this.left = left;
            this.right = right;
        }

        Node(byte[] hash) {
            this(hash, null, null);
        }

        Node join(Node n) {
            byte[] pairHash = Utils.concatBlocks(this.hash, n.hash);
            byte[] hashedPair = Utils.sha256(pairHash);
            Node p = new Node(hashedPair, this, n);
            this.parent = n.parent = p;
            return n;
        }

        public boolean isLeaf() {
    	    if (this.left == null && this.right == null)
		        return true;

	        return false;
        }
    }

    private Node root;
    private Node rightmost;
    private List<byte[]> content;

    public MerkleTree(List<byte[]> content) {
        this.content = content;
        List<Node> leaves = new LinkedList<>();
        for (byte[] bytes : content) {
            Node newLeaf = new Node(bytes);
            leaves.add(newLeaf);
            this.rightmost = newLeaf;
        }
        this.root = joinNodes(leaves);
    }

    public MerkleTree() {
    	this(null);
    }

    private Node joinNodes(List<Node> nodes) {
        if (nodes == null || nodes.size() == 0) {
            return null;
        }
        if (nodes.size() == 1) {
            return nodes.get(0);
        }
        List<Node> newLayer = joinLayer(nodes);
        return joinNodes(newLayer);
    }

    private List<Node> joinLayer(List<Node> layer) {
        /* check that layer is not empty */
	if (layer == null || layer.size() == 0) {
		throw new IllegalArgumentException();
	}
        List<Node> newLayer = new LinkedList<>();
        for (int i = 0; i < layer.size(); i += 2) {
            Node joined;
            try {
                Node firstNode = layer.get(i);
                Node secondNode = layer.get(i + 1);
                joined = firstNode.join(secondNode);
            }
            catch (IndexOutOfBoundsException e) {
                joined = layer.get(i);
            }
            newLayer.add(joined);
        }
        return newLayer;
    }

    public void add (byte[] block)
    {
	if (block == null) return;

        Node n = new Node(Utils.sha256(block));
        if (root == null) {
            root = n;
            rightmost = n;
            content = new ArrayList<>();
            return;
        }
        content.add(block);
        addHelper(rightmost, n);
        rightmost = n;
    }

    private void updateHash(Node n) {
        if (n == null || n.isLeaf()) {
            return;
        }
        if (n.right != null) {
            n.hash = Utils.sha256(Utils.concatBlocks(n.left.hash, n.right.hash));
            updateHash(n.parent);
        }
        else {
            n.hash = Utils.sha256(n.left.hash);
            updateHash(n.parent);
        }

    }

    private void addHelper (Node current, Node newNode) {
        if (current.parent == null) {
            root = current.join(newNode);
        }
        else if (current.parent.right == null) {
            current.parent.right = newNode;
            newNode.parent = current.parent;
            updateHash(current.parent);
        }
        else {
            Node newParent = new Node(Utils.sha256(newNode.hash));
            newNode.parent = newParent;
            newParent.left = newNode;
            addHelper(current.parent, newParent);
        }
    }

    @Override
    public String toString() {
        return "MerkleTree{}";
    }
}
