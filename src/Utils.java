import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

class Utils {

    static byte[] sha256(byte[] block) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            return digest.digest(block);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

    static byte[] concatBlocks(byte[] b1, byte[] b2) {
        byte[] newBlock = new byte[b1.length + b2.length];
        System.arraycopy(b1, 0, newBlock, 0, b1.length);
        System.arraycopy(b2, 0, newBlock, b1.length, b2.length);
        return newBlock;
    }
}
